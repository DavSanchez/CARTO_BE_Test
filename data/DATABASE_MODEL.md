# Database model

The database model mirrors the data as presented in the `csv` files, stripping the table and column names from any special characters. It assumes that the `postal_code_id` column of the `paystats.csv` file references the `id` column of `postal_codes.csv`.

To maintain referential integrity, this involves setting `postal_code_id` as a foreign key, referencing the postal code `id` as show in the diagram below.

**IMPORTANT:** See [./datasets/SANITIZING_DATASETS.md](./datasets/SANITIZING_DATASETS.md)

![Database relations](./db_model.png "Database relations")
