# About the pre-sanitizing of the dataset files

Some entries in `paystats.csv` contained postal code IDs that are not present in `postal_codes.csv`. As such, they don't have any associated geometry, and provoke a **foreign key constraint violation** in the database tables as they are currently modeled (assuring postal code referential integrity).

The decision taken was to sanitize the `paystats.csv` dataset, deleting every line that contained the offending postal code IDs, and saving a newfile as `paysats_sanitized.csv`:

```python
import csv

# Postal code IDs from postal_codes.csv
p_code_ids = set()

with open('postal_codes.csv', 'r') as p_codes_file:
  # Ignore first line
  next(csv.reader(p_codes_file))

  for row in csv.reader(p_codes_file):
    # Add postal code IDs to set
    p_code_ids.add(row[2])

with open('paystats.csv', 'r') as inp, open('paystats_sanitized.csv', 'w') as out:
  # Ignore first line
  next(csv.reader(inp))
  
  # Write first line in the new file
  csv.writer(out).writerow(['amount','p_month','p_age','p_gender','postal_code_id','id'])

  # Only write line if postal code exists in p_codes
  for row in csv.reader(inp):
    if row[4] in p_code_ids:
      csv.writer(out).writerow(row)
    else:
      invalid_p_code_ids.add(row[4])
```

## Note

**This decision shrinked `paystats` from 10652 rows to 4524, so the situation would need further and precise reviewing!**

In the end, it boils down to what importance does the project give to data without an associated geometry. Are the postal code areas the one and only feature the user will interact with, and all the visible data must come from these regions (including the ones at the left side view)? Then it makes sense to remove this _incomplete_ data. On the contrary, if the project includes making use of data independent of geometry, then it would not make sense to remove it.

At the moment, and taking into account the read frontend requirements, and this being a GIS application, the decision has been to remove the data anyway.

## Offending postal code IDs
  - 6062
  - 6065
  - 6066
  - 6068
  - 6070
  - 6109
  - 6115
  - 6125
  - 6126
  - 6127
  - 6152
  - 6156
  - 6172
  - 6173
  - 6174
  - 6175
  - 6178
  - 6190
  - 6199
  - 6208
  - 6209
  - 6212
  - 6213
  - 6216
  - 6217
  - 6237
  - 6238
  - 6259
  - 6261
  - 6271
  - 6272
  - 6273
  - 6283
  - 6334
  - 6336
  - 6353
  - 6358
  - 6359
  - 6360
  - 6382
  - 6388
  - 6390
  - 6396
  