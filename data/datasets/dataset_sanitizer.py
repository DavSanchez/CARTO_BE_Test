import csv

# Postal code IDs from postal_codes.csv
p_code_ids = set()

# For storing the offending postal code IDs
invalid_p_code_ids = set()

with open('postal_codes.csv', 'r') as p_codes_file:
    # Ignore first line
    next(csv.reader(p_codes_file))

    for row in csv.reader(p_codes_file):
        # Add postal code IDs to set
        p_code_ids.add(row[2])

with open('paystats.csv', 'r') as inp, open('paystats_sanitized.csv', 'w') as out:
    # Ignore first line
    next(csv.reader(inp))

    # Write first line in the new file
    csv.writer(out).writerow(
        ['amount', 'p_month', 'p_age', 'p_gender', 'postal_code_id', 'id'])

    # Only write line if postal code exists in p_codes
    for row in csv.reader(inp):
        if row[4] in p_code_ids:
            csv.writer(out).writerow(row)
        else:
            invalid_p_code_ids.add(row[4])

print('Offending postal code IDs:\n')

for invalid_code in sorted(invalid_p_code_ids):
    print('  - ' + invalid_code)
