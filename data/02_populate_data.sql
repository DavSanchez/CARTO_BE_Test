BEGIN;
  -- Import postal_codes.csv
  COPY postalcodes(geom, code, id)
  FROM '/docker-entrypoint-initdb.d/datasets/postal_codes.csv' DELIMITERS ',' CSV HEADER;

  -- Set SRID to 4326 (WSG84)
  UPDATE postalcodes SET geom = ST_SetSRID(geom,4326);

  -- Import paystats.csv
  COPY paystats(amount, month, age, gender, postalcode, id)
  FROM '/docker-entrypoint-initdb.d/datasets/paystats_sanitized.csv' DELIMITERS ',' CSV HEADER;
COMMIT;
