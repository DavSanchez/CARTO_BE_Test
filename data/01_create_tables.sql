CREATE TABLE postalcodes
(
  geom GEOMETRY,
  code INT,
  id  INT NOT NULL PRIMARY KEY
);

CREATE TABLE paystats
(
  amount DOUBLE PRECISION,
  month DATE,
  age VARCHAR(10),
  gender CHAR(1),
  postalcode INT REFERENCES postalcodes(id),
  id INT NOT NULL PRIMARY KEY
);
