import os

# Load .env file if it exists
try:
    from dotenv import load_dotenv
    load_dotenv()
except:
    pass


class Config(object):
    # Environment variables and additional config stuff
    DB_USER = os.environ.get("DB_USER") or "docker"
    DB_PASS = os.environ.get("DB_PASS") or "docker"
    DB_HOST = os.environ.get("DB_HOST") or "postgis"
    DB_PORT = os.environ.get("DB_PORT") or "5432"
    DB_NAME = os.environ.get("DB_NAME") or "gis"
