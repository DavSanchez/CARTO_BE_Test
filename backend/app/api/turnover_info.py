from flask import jsonify, request

from app.api import bp, errors
from .. import db as database


@bp.route("/totalturnover", methods=["GET"])
def total_turnover():
    """
    Get the total aggregated turnover to display on frontend.

    Will return: { "totalTurnover": number }
    """
    try:
        result = {"totalTurnover": 0}

        db = database.get_db()
        cursor = db.cursor()

        # This should return a list of tuples
        cursor.execute("SELECT amount FROM paystats;")

        if cursor.rowcount is 0:
            cursor.close()
            return jsonify(result)

        amount_tuple = cursor.fetchall()

        # Convert to list, sum all values and round to 2 decimals (monetary value)
        result["totalTurnover"] = round(sum([i[0] for i in amount_tuple]), 2)

        cursor.close()
        return jsonify(result)

    except Exception as error:
        cursor.close()
        errors.error_response(500, message=error)


@bp.route("/turnovertimeseriesbyage", methods=["GET"])
def turnover_time_series():
    """
    Get male and female turnovers for a certain age interval, postal code and
    month, for making time series.

    Must process query strings: age_interval, postal_code.

    Example:
    Turnovers with gender and date for ages <=24 and postal code 28041:
    GET /api/turnovertimeseriesbyage?age_interval=%3C%3D24&postal_code=28041

    Will return: { "timeSeries": [
                    {
                        "amount": number,
                        "gender": "M/F",
                        "date": "2020-02-16",
                    },
                    {
                        # ...
                    }]
                }
    """
    try:
        result = {"timeSeries": []}

        age_interval = request.args.get("age_interval")
        postal_code = request.args.get("postal_code")

        db = database.get_db()
        cursor = db.cursor()

        cursor.execute("""
        SELECT (paystats.amount, paystats.gender, paystats.month) FROM paystats
        INNER JOIN postalcodes ON postalcodes.id=paystats.postalcode
        WHERE postalcodes.code=%s AND paystats.age=%s
        ORDER BY paystats.month DESC;
        """, (postal_code, age_interval))

        if cursor.rowcount is 0:
            cursor.close()
            return jsonify(result)

        # Tuple received is of the form ("(amount,M/F,date)",)
        for record in cursor:
            res = tuple(record[0].strip("()").split(","))
            result["timeSeries"].append(
                {
                    "amount": res[0],
                    "gender": res[1],
                    "date": res[2],
                }
            )

        cursor.close()

        return jsonify(result)

    except Exception as error:
        cursor.close()
        errors.error_response(500, message=error)


@bp.route("/turnoveragegender", methods=["GET"])
def turnover_by_age_and_gender():
    """
    Get total aggregated turnover by age and gender.
    """
    # TODO: not implemented, stub
    return {
        "turnoverAgeGender": [
            {
                "ageInterval": "<=24",
                "female": 123456,
                "male": 123456,
            },
            {
                "ageInterval": "25-34",
                "female": 456789,
                "male": 456789,
            },
        ]
    }


@bp.route("/allageintervals", methods=["GET"])
def all_age_intervals():
    """
    Get all age intervals for the data present in the database,
    for use in frontend filters.

    This would be provisional for the database as it currently is.
    Wise use of PostgreSQL data types for the database can make
    this endpoint unneeded.
    """
    # TODO: not implemented, stub
    return {
        "ageIntervals": ["<=24", "25-34", "35-44", "..."]
    }
