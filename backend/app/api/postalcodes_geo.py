from flask import jsonify
import geojson

from app.api import bp, errors
from .. import db as database


@bp.route("/pcgeo", methods=["GET"])
def postal_codes_geo():
    """
    Get all postal codes and their geometries to display at frontend.
    """
    try:
        result = {"pcgeo": []}

        db = database.get_db()
        cursor = db.cursor()

        # This should return a list of tuples
        cursor.execute(
            """
            SELECT (code,ST_AsGeoJSON(ST_Transform(geom,4326))) FROM postalcodes;
            """
        )

        if cursor.rowcount is 0:
            cursor.close()
            return jsonify(result)

        # Tuple received is of the form ("(postalcode,geometry)",)
        for record in cursor:
            res = tuple(record[0].strip("()").split(",", maxsplit=1))
            """
            FIXME: The GeoJSON data comes out like this (why?): 
                "\"{\"\"type\"\":\"\"MultiPolygon\"\",\"\"coordinates\"\":[ ...
            Therefore a sloppy (and I hope temporal) fix is needed in pcGeometry.
            """
            result["pcgeo"].append(
                {
                    "postalCode": int(res[0]),
                    "pcGeometry": geojson.loads(res[1].replace('""', '"').strip('"')),
                }
            )

        cursor.close()
        return jsonify(result)

    except Exception as error:
        cursor.close()
        errors.error_response(500, message=error)
