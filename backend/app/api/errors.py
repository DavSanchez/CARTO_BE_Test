from flask import jsonify
from werkzeug.http import HTTP_STATUS_CODES

"""
API error representation:

{
    "error": "short error description",
    "message": "error message (optional)"
}
"""


def error_response(status_code, message=None):
    payload = {"error": HTTP_STATUS_CODES.get(status_code, "Unknown error")}
    if message:
        payload["message"] = message
    response = jsonify(payload)
    response.status_code = status_code
    return response


def bad_request(message):
    """
    For error 400
    """
    error_response(400, message)
