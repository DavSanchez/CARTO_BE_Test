import os

from flask import Flask, jsonify
from config import Config


def create_app():
    """
    Application factory, which creates the app, loads its configuration
    and registers the database handler and the API blueprint. 
    """
    # Create and configure the app
    application = Flask(__name__, instance_relative_config=True)
    application.config.from_object(Config)

    # Ensure the instance folder exists
    try:
        os.makedirs(application.instance_path)
    except OSError:
        pass

    # Database handler
    from . import db
    db.init_app(application)

    # API blueprint
    from app.api import bp as api_bp
    application.register_blueprint(api_bp, url_prefix="/api")

    # A simple route to manually test that Flask + PostgreSQL is working
    # TODO: Remove for production
    @application.route("/test")
    def test_app():
        pg = db.get_db()
        cursor = pg.cursor()
        cursor.execute("SELECT version();")
        result = cursor.fetchone()
        print(result)

        cursor.close()
        return jsonify(result)

    return application
