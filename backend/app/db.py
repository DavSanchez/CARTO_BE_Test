import psycopg2
import time
import click

from flask import current_app, g

"""
  With the current configuration, the Flask app will probably be up before PostgreSQL is ready
  to accept connections. So, we try connecting once every 5 seconds until we succeed.
"""


def get_db():
    try:
        g.db = psycopg2.connect(user=current_app.config["DB_USER"],
                                password=current_app.config["DB_PASS"],
                                host=current_app.config["DB_HOST"],
                                port=current_app.config["DB_PORT"],
                                database=current_app.config["DB_NAME"])
    except (Exception, psycopg2.Error) as error:
        print("Error while connecting to PostgreSQL", error)
    finally:
        return g.db


def close_db(e=None):
    db = g.pop("db", None)

    if db is not None:
        db.close()
        print("PostgreSQL connection is closed")


def init_db():
    while 1:
        try:
            print("Attempting connection to PostgreSQL database")
            db = get_db()
        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            print("Waiting for database...")
            time.sleep(5)
        else:
            # Print PostgreSQL Connection properties
            print(db.get_dsn_parameters(), "\n")
            cursor = db.cursor()
            cursor.execute("SELECT version();")
            record = cursor.fetchone()
            print("You are connected to - ", record, "\n")
            cursor.close()
            break


def init_app(app):
    with app.app_context():
        init_db()

    app.teardown_appcontext(close_db)
