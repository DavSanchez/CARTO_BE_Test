# CARTO Backend Test

This repo contains my proposed solution to [this backend test by CARTO](https://gist.github.com/ajaest/164cbf99777cc33463b20e3e2fce9313).

## App features

### Architecture

The solution proposed makes use of a [PostGIS-enabled database](https://github.com/kartoza/docker-postgis) and a [daily backup service](https://github.com/kartoza/docker-pg-backup) provided by [preconfigured Docker containers architecture via `docker-compose`](https://github.com/kartoza/docker-postgis/blob/develop/docker-compose.yml), which was modified to suit the project needs. Added to this architecture is the API backend service, coded using Flask and also enabled to run as a Docker container.

This configuration makes possible to setup the whole architecture with one command, executed from the project's root directory:

```sh
docker-compose up -d --build
```

Where the option `-d` makes `docker-compose` run in detached mode and `--build` rebuilds the Python backend in case changes were made since the last setup.

To shutdown the whole architecture, a similar command needs to be used from the project's root directory:

```
docker-compose down
```

This will stop all the containers but won't remove the volumes defined for them, the database contents will be persisted in its volume.

![Project architecture](./be_architecture.png "Project architecture")

As the above diagram suggest, it is possible to run other backends external to the Docker container architecture, provided the different configuration parameters needed (backend port, database host and ports...) are taken into account.

The following libraries were used for the backend development:
- `flask`: Web application framework to set up the API.
- `python-dotenv`: For getting environment variables from an `.env` file when running the backend outside Docker.
- `psycopg2`: PostgreSQL database driver for queries.
- `geojson`: For decoding GeoJSON data.

### Directory structure

The project's root directory contains all the files and directory structure needed to run the whole architecture (`docker-compose.yml`) and also maintain some separation of concerns (`.gitignore`, `.dockerignore`, `.env`, etc). The Python backend resides in the `backend` directory, while the data used to initialize the database and the original datasets are stored in `data`.

### Database initialization

To setup the database prior to working with it, two `sql` files where created and put into the `data` directory. They make use of the datasets provided and store their contents in the database according to the [proposed model](./data/DATABASE_MODEL.md).

Before populating the tables, though, [some sanitizing was deemed necessary](./data/datasets/SANITIZING_DATASETS.md).

The `data` directory was then mounted to a specific directory within the `postgis` container (`/docker-entrypoint-initdb.d`, check [`docker-compose.yml`](https://gist.github.com/ajaest/164cbf99777cc33463b20e3e2fce9313)) to make the database execute them the first time it runs.

## API endpoints

The API was implemented making use of Flask Blueprints, which allow registering the in the defined Flask application factory function and define url prefixes. All API endpoints return a JSON object, the details below define its content for each endpoint.

### `GET /api/pcgeo`
This endpoint is used to get all the postal code geometries for representing in a GIS-enabled frontend. It returns an array, where each element declares a postal code (integer number) and its geometry (GeoJSON):

```json
{
    "pcgeo": [
        {
            "postalCode": 000000,
            "pcGeometry": "<GeoJSON object>"
        },
        // ...
    ]
}
```

#### Possible endpoint improvements
This endpoint makes use of a database query that involves getting the data as GeoJSON. Currently, this query returns what seems to me as a bad formatted string, so a workaround was used to sanitize this string and parse it (via the `geojson` module) before sending the response. This needs improvement.

### `GET /api/allageintervals`
This endpoint is used to get all the page intervals as they were defined in the dataset (and thus in the database model). It returns an array of strings:

```json
{
    "ageIntervals": [
        "<=24",
        "25-34",
        // ...
    ]
}
```
#### Possible endpoint improvements
This would be provisional for the database as it currently is. Wise use of PostgreSQL data types for the database, coupled with more accurate data from the datasets, could make this endpoint unneeded.

### `GET /api/totalturnover`
This endpoint is used to get the total aggregated turnover for all the postal codes. It returns a floating point number rounded to two decimal places (as it represents a monetary value):

```json
{ "totalTurnover": 123456789.12 }
```

### `GET /api/turnovertimeseriesbyage`
Gets male and female turnovers for a certain age interval and postal code, ordered by date (descending). Useful for representing time series (bottom left chart in the frontend wireframe).

Must processes query strings: `age_interval`, `postal_code`.

**Example:**
> Turnovers with gender and date for ages <=24 and postal code 28668:
>
> `GET /api/turnovertimeseriesbyage?age_interval=%3C%3D24&postal_code=28668`

```json
{
    "timeSeries": [
        {
            "amount": 12345,
            "gender": "F",
            "date": "2020-02-16",
        },
        // ...
    ]
}
```

### `GET /api/turnoveragegender`
Gets aggregated male and female turnovers for every age range, ordered by date (descending). Useful for bar charts (top left chart in the frontend wireframe).

```json
{
    "turnoverAgeGender": [
        {
            "ageInterval": "<=24",
            "female": 123456,
            "male": 123456,
        },
        {
            "ageInterval": "25-34",
            "female": 456789,
            "male": 456789,
        },
        // ...
    ]
}
```

## Further improvements

### Database handling
The database handler is not fully decoupled from the rest of the code. Maybe `g.db` should store the cursor instead of the connection, but then I would run into problems to close the connection itself in `close_db()` (Flask app context teardown). Check [psycopg2 documentation](https://www.psycopg.org/docs/cursor.html#cursor.connection) for a possible way to close the connection from the cursor.

### Database model and behaviour
As suggested on several API endpoints above, the database model could be improved to enforce business domain needs and potentially allow more fine-tuning (for example, via the correct use of data types), and also to solve current issues related to the representation of GeoJSON output.

### ... and much more!
I mean, this was done in less than half a day in total! Sure there is a lot to troubleshoot :)
